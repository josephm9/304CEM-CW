'use strict'

const schema = require('./schema/schema.js')

/**promise to find trail centre in mongodb
*
*@param {String} centre - String containing centre name
*/
exports.findCentre = (centre) => new Promise ( (resolve, reject) => {
  let places = {
    "cannock": "cannockchase",
    "afan": "afan",
    "llandegla": "llandegla"
  }

  schema.Centre.find({hiddenName: places[centre.centre]}, (err, docs) => {
    if (err) {
      reject (new Error('Centre not found.'))
    }

    if (docs.length) {
      let data = {}
      let temp = docs[0]
      data.name = temp.name
      data.address = temp.address
      data.website = temp.website
      data.bikeHire = temp.bikeHire
      data.trails = temp.trails

      resolve(data)
  }
  reject (new Error("Database Error"))
  })
})

/**promise to add trail centre to mongodb
*
*@param {object} details - JSON containing new trail centre details
*/
exports.addCentre = details => new Promise ( (resolve, reject) => {
  if (!details) {
    reject(new Error("no input given"))
  }
  if (!('hiddenName' in details) || !('name' in details) || !('address' in details) ||
  !('website' in details) || !('bikeHire' in details) || !('trails' in details)) {
    reject (new Error(details))
  }
    else {
      const centre = new schema.Centre(details)
      centre.save((err, user) => {
        if (err) {
          reject (new Error('error adding centre'))
        }
        resolve(details)
      })
}})

/**promise to add user to mongodb
*
*@param {object} details - JSON containing user details
*/
exports.addUser = details => new Promise( (resolve, reject) => {
  if(!'username' in details && !'password' in details && !'name' in details) {
    reject(new Error('invalid username/password'))
  }
  const user = new schema.User(details)

  user.save((err, user) => {
    if (err) {
      reject(new Error('error during account creation'))
    }
    delete details.password
    resolve(details)
  })
})

/**promise to get user credentials from input and find them in database
*
*@param {object} credentials - JSON containing user credentials
*/
exports.getCredentials = credentials => new Promise ((resolve, reject) => {
  schema.User.find({username: credentials.username}, (err, docs) => {
    if (err) {
      reject(new Error('database error'))
    }
    if (docs.length) {
      resolve(docs)
    }
    reject(new Error('invalid username'))
  })
})

/**promise to take input and check whether the account exists in the database
*
*@param {object} account - JSON containing account details to check against database
*/
exports.accountExists = account => new Promise( (resolve, reject) => {
  schema.User.find({username: account.username}, (err, docs) => {
    if (err) {
      reject(new Error('database error'))
    }
    if (docs.length) {
      reject(new Error('username taken'))
      resolve()
    }
  })
})
