'use strict'

const request = require('request')

exports.weather = (centre) => new Promise( (resolve, reject) => {

  let places = {
    "cannock": "Cannock",
    "afan": "Port Talbot",
    "llandegla": "Llandegla"
  }

  if (!places.hasOwnProperty(centre.centre)) {
    reject(new Error("not a valid trail centre"))
  }

  let location = places[centre.centre]

  const url = `http://api.openweathermap.org/data/2.5/forecast/city?q=${places[centre.centre]}&units=metric&APPID=190ebf3f3443037d14d6c8cb86ea8b01`

  request.get(url, (err,res,body) => {
    if (err){
      throw 'request not completed'
    }
    const json = JSON.parse(body)
    const list = json.list
    const output = {weather: []}
    for (let step = 0; step <30; step++) {
      let tempObject = {}
      tempObject.date = list[step].dt_txt
      tempObject.temp = list[step].main.temp
      tempObject.desc = list[step].weather[0].description
      output.weather[step] = tempObject
    }

    resolve(output)
  })
})
