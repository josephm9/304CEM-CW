'use strict'

const bcrypt = require('bcrypt')

/**promise to extract username and password from http header
*
*@param {object} request - JSON containing http header
*/
exports.getHeaderCredentials = request => new Promise( (resolve, reject) => {

  if (request.authorization === undefined || request.authorization.basic === undefined) {
    reject(new Error("authorization header missing"))
  }
  const auth = request.authorization.basic

  if (auth.username === undefined || auth.password === undefined) {
    reject(new Error('missing username and/or password'))
  }
  resolve({username: auth.username, password: auth.password})
})

/**promise to hash the password
*
*@param {object} credentials - JSON containing credentials
*/
exports.hashPassword = credentials => new Promise( (resolve, reject) => {
  const salt = bcrypt.genSaltSync(10)
  let hash = bcrypt.hashSync(credentials.password, salt)
  credentials.password = hash
  resolve(credentials)
})

/**promise to check input password against stored password
*
*@param {string} provided - String containing provided password
*@param {string} stored - String containing password from database
*/
exports.checkPassword = (provided, stored) => new Promise( (resolve, reject) => {
  if(!provided === stored) {
    reject(new Error(bcrypt.compareSync(provided, stored)))
  }
  resolve()
})
