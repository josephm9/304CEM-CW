'use strict'

const persistence = require('./persistence.js')
const weather = require('./weather.js')
const auth = require('./authorisation.js')

/**Calls promise to pull information about trail centre from mongodb
*
*@param {string} centre - String containing centre name
*@param callback - callback
*/
exports.information = function(centre, callback) {
  /*persistence.addCentre({
  "hiddenName": "cannockchase",
  "name": "Cannock Chase",
  "address": "Birches Valley Forest Centre - WS15 2UQ",
  "website": "http://www.chasetrails.co.uk/",
  "bikeHire": [
    {
      "hardtail": true,
      "fullSuspension": true
    }
  ],
  "trails": [
    {
      "name": "Fair Oak Pools",
      "length": 4.02,
      "grade": "green"
    },
    {
      "name": "Sherbrook Valley Short",
      "length": 12.07,
      "grade": "blue"
    },
    {
      "name": "Sherbrook Valley Short",
      "length": 20.12,
      "grade": "blue"
    },
    {
      "name": "Follow the Dog",
      "length": 10.83,
      "grade": "red"
    },
    {
      "name": "The Monkey Trail",
      "length": 22.84,
      "grade": "red"
    }
  ]
})*/
  persistence.findCentre(centre)
  .then(data => {
    callback(null,data)

  }).catch((err) => {
    callback(err)
  })
}

/**calls promise to add a new trail centre to the mongodb
*
*@param {object} input - JSON containing centre information
*@param callback - callback
*/
exports.addCentre = (input, callback) => {
  auth.getHeaderCredentials(input).then(credentials => {
    this.username = credentials.username
    this.password = credentials.password
    return auth.hashPassword(credentials)
  }).then(credentials => {
    persistence.getCredentials(credentials)
  }).then (account => {
    const hash = account[0].password
    auth.checkPassword(this.password, hash)
  }).then( (data) => {
    persistence.addCentre(input.body)
  }).then (data => {
    callback(null, data)
  }).catch ((err) => {
    callback(err)
  })
}

/**calls promise to make a call to the weather api for the specified trail centre
*
*@param {string} centre - String containing centre name
*@param callback - callback
*/
exports.weather = function(centre, callback) {
  weather.weather(centre)
  .then (data => {
    callback(null,data)

  }).catch ((err) => {
    callback(err)
  })
}

/**calls promise to add a user to the mongodb
*
*@param {object} request - JSON containing http request
*@param callback - callback
*/
exports.addUser = (request, callback) => {
  auth.getHeaderCredentials(request).then(credentials => {
    this.username = credentials.username
    this.password = credentials.password
    return auth.hashPassword(credentials)
  }).then(credentials => {
    return persistence.getCredentials(credentials)
  }).then (account => {
    const hash = account[0].password
    return auth.checkPassword(this.password, hash)
  }).then( name => {
    return persistence.addUser(request.body)
  })
  .then(data => {
    callback(null, data)
  }).catch((err) => {
    callback(err)
  })
}
