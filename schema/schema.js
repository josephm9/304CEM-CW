'use strict'

const mongoose = require('mongoose')

mongoose.connect('mongodb://user:userpass@ds159387.mlab.com:59387/trail_centres')
mongoose.Promise = global.Promise
const Schema = mongoose.Schema

const centreSchema = new Schema({
  hiddenName: String,
  name: String,
  address: String,
  website: String,
  bikeHire: [{hardtail: Boolean, fullSuspension: Boolean}],
  trails: [{hiddenName: String, name: String, length: Number, grade: String}]
})

exports.Centre = mongoose.model('Centre', centreSchema)

const userSchema = new Schema({
  name: String,
  username: String,
  password: String
})

exports.User = mongoose.model('User', userSchema)
