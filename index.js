'use strict'

const restify = require('restify')
const centre = require('./centre.js')

const server = restify.createServer()

server.use(restify.acceptParser(server.acceptable))
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.fullResponse())
server.use(restify.authorizationParser())

const status = {
  ok: 200,
  added: 201,
  badRequest: 400
}

server.listen(8080, function() {
    console.log("Server started on port 8080");
});

server.get('/centres/:centre', (req, res) => {
  centre.information (req.params, (err, data) => {
    res.setHeader('content-type','application/json')
    res.setHeader('accepts','GET')
    if (err) {
      res.send(status.badRequest, {error: err.message})
    } else {
      res.send(status.added, {data})
    }
    res.end()
  })
})

server.get('/centres/:centre/weather', (req, res) => {
  centre.weather (req.params, (err, data) => {
    res.setHeader('content-type', 'application/json')
    res.setHeader('accepts','GET')
    if (err) {
      res.send(status.badRequest, {error: err.message})
    } else {
      res.send(status.added, {data})
    }
    res.end()
  })
})

server.post('/users', (req, res) => {
  centre.addUser(req, (err, data) => {
    res.setHeader('content-type', 'application/json')
    res.setHeader('accepts', 'GET, POST')
    if (err) {
      res.send(status.badRequest, {error: err.message})
    } else {
      res.send(status.added, {user: data})
    }
    res.end()
  })
})

server.post('/centres', (req, res) => {
  centre.addCentre(req, (err, data) => {
    res.setHeader('content-type', 'application/json')
    res.setHeader('accepts', 'POST')
    if (err) {
      res.send(status.badRequest, {error: err.message})
    } else {
      res.send(status.added, {centre: data})
    }
  })
})
