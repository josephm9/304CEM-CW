'use strict'

const request = require('request')
const persistence = require('../persistence.js')

describe("Centre call", function() {
  it("Should respond with centre data", function(done) {
    request("http://localhost:8080/centres/cannock", function(err, response, body){
      let json = JSON.parse(body)
      expect(json.data.name).toBe("Cannock Chase")
      done()
    })
  })

  it("Should respond with centre error", function(done) {
    request("http://localhost:8080/centres/nonexistant", function(err, response, body){
      let json = JSON.parse(body)
      expect(json.error).toBe("Database Error")
      done()
    })
  })
})

describe("Weather call", function() {
  it("Should respond with weather at centre", function(done) {
    request("http://localhost:8080/centres/cannock/weather", function(err, response, body){
      let json = JSON.parse(body)
      expect(json.data.weather).toBeDefined()
      done()
    })
  })
  it("Should respond with weather error", function(done) {
    request("http://localhost:8080/centres/nonexistant/weather", function(err, response, body){
      let json = JSON.parse(body)
      expect(json.error).toBe("not a valid trail centre")
      done()
    })
  })
})
